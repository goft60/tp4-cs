using System;
using Xunit;

using Console;
namespace Test
{
    public class UnitTest1
    {
        [Fact]
        public void Carre()
        {
            Assert.Equal(4,Program.AireCarre(2));
            Assert.Equal(16,Program.AireCarre(4));
            Assert.Equal(1,Program.AireCarre(1));
            Assert.Equal(100,Program.AireCarre(-10));
        }
        [Fact]
        public void Triangle()
        {
            Assert.Equal(3,Program.AireTriangle(2,3));
            Assert.Equal(0,Program.AireTriangle(0,5));
            Assert.Equal(25,Program.AireTriangle(10,5));
            Assert.Equal(15,Program.AireTriangle(-10,-3));
        }
        [Fact]
        public void Rectangle()
        {
            Assert.Equal(6,Program.AireRectangle(2,3));
            Assert.Equal(0,Program.AireRectangle(0,5));
            Assert.Equal(50,Program.AireRectangle(10,5));
            Assert.Equal(30,Program.AireRectangle(-10,-3));
        }
        [Fact]
        public void Majorite()
        {
            Assert.Equal(1,Program.MajoritebyYear(2000));
            Assert.Equal(0,Program.MajoritebyYear(2002));
            Assert.Equal(0,Program.MajoritebyYear(2054));
        }
        [Fact]
        public void IntinArray()
        {
            Assert.Equal(0,Program.IntInArray(new int [1] {1},1));
            Assert.Equal(3,Program.IntInArray(new int [5] {1,2,3,4,5},4));
            Assert.Equal(-1,Program.IntInArray(new int [5] {1,2,3,4,5},6));
        }
    }
}
