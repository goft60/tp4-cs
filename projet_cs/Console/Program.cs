﻿using System;

namespace Console
{
    public class Program
    {
        public static int AireCarre(int cote)
        {
            if(cote<0)
                cote = -cote;
            return cote*cote;
        }
        public static int AireTriangle(int baseTriangle, int hauteur)
        {
            if(baseTriangle<0)
                baseTriangle = -baseTriangle;
            if(hauteur<0)
                hauteur = -hauteur;
            return baseTriangle*hauteur/2;
        }
        public static int AireRectangle(int longueur, int largeur)
        {
            if(longueur<0)
                longueur = -longueur;
            if(largeur<0)
                largeur = -largeur;
            return longueur*largeur;
        }
        public static int MajoritebyYear(int annee)
        {
            int age=0;
            age = DateTime.Now.Year-annee;
            if(age>=18)
                age=1;
            else
                age=0;
            return age;
        }
        public static int IntInArray(int [] tableau, int nb_search)
        {
            int indice=0;
            while(tableau[indice]!=nb_search && indice < tableau.Length-1)
                indice++;
            if(indice == tableau.Length-1 && tableau[tableau.Length-1]!=nb_search)
                indice=-1;
            return indice;
        }

        static void Main(string[] args)
        {
            
        }
    }
}
